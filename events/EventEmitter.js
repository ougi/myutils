const listenersMap = [];

const obj = {},
  objProto = obj.__proto__;

export default class EventEmitter {
  /**
   * イベントを待ち受ける
   * @param type 受信を待ち受けたいイベントタイプ
   * @param listener 受信した時に実行したい処理
   */
  on(type, listener) {
    let listeners;
    for (let listenersMapItem of listenersMap) {
      if (listenersMapItem.target == this) {
        listeners = listenersMapItem.listeners;
      }
    }
    if (!listeners) {
      listeners = {};
      listenersMap.push({
        target: this,
        listeners
      });
    }

    const types = type.split(" ");
    for (type of types) {
      if (!listeners[type]) listeners[type] = [];
      listeners[type].push(listener);
    }
    return this;
  }

  /**
   * イベント待ち受けを解除する
   * @param type 受信を解除したいイベントタイプ
   * @param listener 受信を解除したい処理
   */
  off(type, listener) {
    let listeners;
    for (let listenersMapItem of listenersMap) {
      if (listenersMapItem.target == this) {
        listeners = listenersMapItem.listeners;
      }
    }
    if (!listeners) return this;

    const types = type.split(" ");
    for (type of types) {
      if (!listeners.hasOwnProperty(type)) return this;

      for (let eventType in listeners) {
        if (type == eventType) {
          if (listener) {
            const typedListeners = listeners[eventType];
            for (let i = typedListeners.length - 1; i >= 0; i--) {
              if (typedListeners[i] == listener) {
                typedListeners.splice(i, 1);
              }
            }
            if (typedListeners.length === 0) {
              delete listeners[eventType];
            }
          } else {
            delete listeners[eventType];
          }
        }
      }
    }

    return this;
  }

  /**
   * イベント発射
   * @param type 送信するイベントタイプ
   */
  emit(type, data) {
    let listeners;
    for (let listenersMapItem of listenersMap) {
      if (listenersMapItem.target == this) {
        listeners = listenersMapItem.listeners;
      }
    }
    if (!listeners) return this;

    if (data !== undefined) {
      if (data.__proto__ === objProto) {
        data.target = data.target || this;
        data.type = data.type || type;
      }
    }

    let callbacks = [];
    for (let eventType in listeners) {
      let eType = eventType;
      if (eventType.indexOf(".") > 0) {
        eType = eventType.split(".")[0];
      }
      if (eType === type) {
        callbacks = callbacks.concat(listeners[eventType]);
      }
    }
    for (let callback of callbacks) {
      callback.call(this, data);
    }
    return this;
  }
}
