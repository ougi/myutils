export default class Vector2 {
  /**
   * コンストラクタ
   * @param {*} x x座標
   * @param {*} y y座標
   */
  constructor(x = 0, y = 0) {
    this.x = x;
    this.y = y;
  }

  /**
   * 距離を計算する
   */
  distance(vec) {
    // ピタゴラスの定理
    return Math.sqrt(Math.pow(vec.x - this.x, 2) + Math.pow(vec.y - this.y, 2));
  }

  /**
   * 角度を計算
   */
  radianTo(vec) {
    const radian = Math.atan2(vec.y - this.y, vec.x - this.x);
    return radian;
  }

  /**
   * 角度からxy座標を計算
   */
  pointTo(radian, distance) {
    let x = distance * Math.cos(radian);
    let y = distance * Math.sin(radian);
    return new Vector2(x, y);
  }

  /**
   * 回転させる
   */
  rotate(radian) {
    const distance = Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
    const afterRadian = Math.atan2(this.y, this.x) + radian;
    const vec2 = new Vector2(
      Math.cos(afterRadian) * distance,
      Math.sin(afterRadian) * distance
    );
    return vec2;
  }

  add(point) {
    this.x += point.x;
    this.y += point.y;
    return this;
  }

  sub(point) {
    this.x -= point.x;
    this.y -= point.y;
    return this;
  }

  multiplyScalar(value) {
    this.x *= value;
    this.y *= value;
    return this;
  }

  divisionScalar(value) {
    this.x /= value;
    this.y /= value;
    return this;
  }

  negate() {
    return this.multiplyScalar(-1);
  }

  clone() {
    return new Vector2(this.x, this.y);
  }

  static distanceTo(vec1, vec2) {
    return Math.sqrt(
      Math.pow(vec1.x - vec2.x, 2) + Math.pow(vec1.y - vec2.y, 2)
    );
  }

  static radianTo(vec1, vec2) {
    const radian = Math.atan2(vec2.y - vec1.y, vec2.x - vec1.x);
    return radian;
  }

  static add(vec1, vec2) {
    return new Vector2(vec1.x + vec2.x, vec1.y + vec2.y);
  }

  static sub(vec1, vec2) {
    return new Vector2(vec1.x - vec2.x, vec1.y - vec2.y);
  }

  static multiplyScalar(vec, value) {
    return new Vector2(vec.x * value, vec.y * value);
  }

  static divisionScalar(vec, value) {
    return new Vector2(vec.x / value, vec.y / value);
  }
}
