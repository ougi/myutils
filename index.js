import Clock from "./utils/Clock";
import ElementUtil from "./utils/ElementUtil";

const $getTimeButton = document.querySelector("#getTimeButton"),
  $clockButton = document.querySelector("#clockButton"),
  $getDeltaButton = document.querySelector("#getDeltaButton"),
  $box = document.querySelector("#box");

let clock;

console.log("boxPosition:", ElementUtil.getPosition($box));

$clockButton.addEventListener("click", () => {
  clock = new Clock();
});

$getTimeButton.addEventListener("click", () => {
  console.log(clock.getTime());
});

$getDeltaButton.addEventListener("click", () => {
  console.log(clock.getDelta());
});

window.addEventListener("scroll", () => {
  console.log("scrollTop:", ElementUtil.getScrollTop());
});
