export default class URLUtil {
  /**
   * マップをクエリ引数に変換する
   */
  static mapToQuery(map) {
    return Object.keys(map)
      .map(key => key + "=" + encodeURIComponent(map[key]))
      .join("&");
  }

  /**
   * クエリ文字列を解析する
   */
  static parseQuery(query) {
    let parameters = {};

    let params = query.replace(/^\?/, "").split("&");
    for (let i in params) {
      let param = params[i].split("=");
      let key = param[0];
      let value = decodeURIComponent(param[1]);
      parameters[key] = value;
    }

    return parameters;
  }

  /**
   * パラメータを取得する
   */
  static getParameters() {
    return this.parseQuery(location.search);
  }

  /**
   * ルートURLを取得する
   */
  static getRootURL() {
    if (location.origin) return location.origin;
    return (
      location.protocol +
      "//" +
      location.hostname +
      (location.port != "80" ? ":" + location.port : "")
    );
  }

  /**
   * クエリパラメータを削除する
   */
  static removeQueryParameters(...removeParameters) {

    let parameters = this.getParameters();
    for (let i = 0; i < removeParameters.length; i++) {
      delete parameters[removeParameters[i]];
    }

    let otherParams = [];
    for (let key in parameters) otherParams.push(key + '=' + encodeURIComponent(parameters[key]));
    let otherParamsString = otherParams.join('&');
    if (otherParamsString) {
      otherParamsString = '?' + otherParamsString;
    }

    // URL を書き換える
    try {
      history.replaceState('', '', location.pathname + otherParamsString);
    } catch (e) {
      try {
        location.href = location.pathname + otherParamsString;
      } catch (e) {
      }
    }

}
