export default class ElementUtil {
  /**
   * 要素の座標を取得
   */
  static getPosition(elem) {
    let x = 0,
      y = 0;
    x = elem.getBoundingClientRect().left + window.pageXOffset;
    y = elem.getBoundingClientRect().top + window.pageYOffset;

    return { x, y };
  }

  /**
   * スクロール位置を取得
   */
  static getScrollTop() {
    let scrollTop = 0;
    if (document.scrollingElement) {
      scrollTop = document.scrollingElement.scrollTop;
    } else {
      scrollTop =
        (document.documentElement && document.documentElement.scrollTop) ||
        document.body.scrollTop;
    }
    return scrollTop;
  }
}
