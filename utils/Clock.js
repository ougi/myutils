export default class Clock {
  constructor() {
    this.start = Date.now();
    this.time = this.start;
  }

  /** 前回実行時からの時間 */
  getDelta() {
    const now = Date.now(),
      delta = now - this.time;
    this.time = now;
    return delta;
  }

  /** 開始時からの時間 */
  getTime() {
    return Date.now() - this.start;
  }
}
