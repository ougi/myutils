import ElementUtil from "../utils/ElementUtil";

let counter = 0;

/**
 * ViewportManager
 *
 * 画面内に表示されたらコールバックを実行する
 */
export default class ViewportManager {
  constructor() {
    this.threshold = 0.2;
    this.enabledRehide = false;

    this._initialized = false;
    this._items = [];
    this._checkId = 0;
  }

  /**
   * 登録
   */
  add(target, callback, invisibleCallback) {
    return this._add(target, callback, invisibleCallback);
  }

  /**
   * 1回だけ実行
   */
  once(elem, callback) {
    return this._add(elem, callback, undefined, true);
  }

  /**
   * 削除
   */
  remove(id) {
    this._items = this._items.filter(value => value.id !== id);
  }

  /**
   * 登録
   */
  _add(target, callback, invisibleCallback, once = false) {
    const id = counter++;
    this._items.push({
      id,
      target,
      callback,
      invisibleCallback,
      once,
      visible: false
    });
    if (this._checkId > 0) cancelAnimationFrame(this._checkId);
    this._checkId = requestAnimationFrame(() => this.check());

    return id;
  }

  /**
   * チェック
   */
  check() {
    if (!this._initialized) {
      this._initialized = true;
      window.addEventListener("scroll", this.check.bind(this));
      window.addEventListener("resize", this.check.bind(this));

      if (this._items.length == 0) return;

      const scrollTop = ElementUtil.getScrollTop();
      const windowHeight = window.innerHeight;

      const threshold = this.threshold;

      for (let item of this._items) {
        const { target } = item;
        let targetElem;
        let targetTop = 0;
        let targetBottom = 0;
        if (typeof target != "number") {
          targetElem = target;
          if (!target.parentNode) {
            if (item.once) {
              this.remove(item.id);
            }
            continue;
          }
          targetTop = ElementUtil.getPosition(target).y;
          targetBottom = targetTop + target.offsetHeight;
        } else {
          targetTop = target;
        }

        const relativeTop = targetTop - scrollTop;
        const relativeBottom = targetBottom - scrollTop;

        if (!item.visible) {
          if (
            windowHeight * (1 - threshold) > relativeTop &&
            0 < relativeBottom
          ) {
            item.visible = true;
            item.callback(targetElem);
            if (item.once) {
              this.remove(item.id);
            }
          }
        } else {
          if (
            this.enabledRehide &&
            (0 > relativeBottom || windowHeight < relativeTop)
          ) {
            item.visible = false;
            if (item.invisibleCallback) {
              item.invisibleCallback(targetElem);
            }
          }
        }
      }
    }
  }
}
